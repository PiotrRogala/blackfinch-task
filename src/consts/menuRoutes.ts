const menuRoutes = {
  home: "/",
  details: "/details/:id",
  cart: "/cart",
};

export default menuRoutes;
