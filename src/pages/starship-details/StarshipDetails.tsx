import "./StarshipDetails.css";

import { useState } from "react";

import Header from "../../components/header/Header";

import { useParams } from "react-router-dom";
import { useStarshipDetails } from "../../hooks/useStarshipDetails";
import {
  Cube,
  Settings,
  DashboardSpeed,
  Minus,
  Plus,
  CartAlt,
} from "iconoir-react";
import { useCart } from "../../hooks/useCart";

const StarshipDetails = () => {
  let { id } = useParams();
  const [amountToAddToCart, setAmountToAddToCart] = useState(0);

  const { starship, loading, error } = useStarshipDetails(id);
  const { addToCart } = useCart();

  return (
    <div>
      <Header />
      {error ? (
        <p>Error : {error}</p>
      ) : loading ? (
        <p>Loading...</p>
      ) : starship && starship.url ? (
        <>
          <h1 className="starship-header">{starship?.name}</h1>
          <div className="starship-details-container">
            <div>
              <p className="starship-details-row">
                <Cube />
                model: {starship?.model}
              </p>
              <p className="starship-details-row">
                <Settings />
                manufacturer: {starship?.manufacturer}
              </p>
              <p className="starship-details-row">
                <DashboardSpeed />
                speed: {starship?.max_atmosphering_speed}
              </p>
            </div>
            <div>
              <img
                className="starship-detail-image"
                src="/ship.png"
                alt={starship.name}
              />
            </div>
          </div>
          <div className="starship-add-to-cart-container">
            <button
              onClick={() => setAmountToAddToCart((value) => value - 1)}
              disabled={amountToAddToCart < 1}
            >
              <Minus />
            </button>
            <span>{amountToAddToCart}</span>
            <button onClick={() => setAmountToAddToCart((value) => value + 1)}>
              <Plus />
            </button>
            <button
              className="add-to-cart-button"
              onClick={() => {
                addToCart(starship.name, amountToAddToCart);
                setAmountToAddToCart(0);
              }}
            >
              <CartAlt />
              Add to cart
            </button>
          </div>
        </>
      ) : (
        <p>Starship not found!</p>
      )}
    </div>
  );
};

export default StarshipDetails;
