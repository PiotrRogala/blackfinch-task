import "./StarshipCard.css";

import { type Starship } from "../../hooks/useStarships";
import { Cube, Settings, DashboardSpeed } from "iconoir-react";
import { Link } from "react-router-dom";

import menuRoutes from "../../consts/menuRoutes";

interface PropTypes {
  starship: Starship;
}

const starshipUrlToId = (url: string | undefined) => {
  return url?.match(/\d+/)?.[0];
};

const StarshipCard = ({ starship }: PropTypes) => (
  <div className="starship-card">
    <Link
      to={menuRoutes.details.replace(
        ":id",
        starshipUrlToId(starshipUrlToId(starship.url)) || ""
      )}
      className="starship-link"
    >
      <h1 className="starship-title">{starship.name}</h1>
      <p className="starship-row">
        <Cube /> {starship.model}
      </p>
      <p className="starship-row">
        <Settings /> {starship.manufacturer}
      </p>
      <p className="starship-row">
        <DashboardSpeed /> {starship.max_atmosphering_speed} km/h
      </p>

      <img className="starship-image" src="/ship.png" alt={starship.name} />
    </Link>
  </div>
);
export default StarshipCard;
