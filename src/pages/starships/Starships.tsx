import "./Starships.css";

import { useState } from "react";

import { useStarships } from "../../hooks/useStarships";
import Header from "../../components/header/Header";
import StarshipCard from "../../components/starship-card/StarshipCard";
import Pagination from "../../components/pagination/Pagination";

const STARSHIPS_PER_PAGE = 10;

const Starships = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const { starships, error, loading, count } = useStarships(currentPage);
  return (
    <div>
      <Header />
      <div className="starships-container">
        {error ? (
          <p> {error}</p>
        ) : loading ? (
          <p>Loading...</p>
        ) : starships && starships.length ? (
          starships.map((starship) => (
            <StarshipCard starship={starship} key={starship.url} />
          ))
        ) : null}
      </div>
      {starships && starships.length ? (
        <Pagination
          currentPage={currentPage}
          pages={count ? Math.ceil(count / STARSHIPS_PER_PAGE) : 0}
          onPageChange={setCurrentPage}
        />
      ) : null}
    </div>
  );
};

export default Starships;
