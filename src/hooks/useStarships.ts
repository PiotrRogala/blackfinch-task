import axios from "axios";

import { useEffect, useState } from "react";
import { baseUrl } from "../consts/api";

export interface Starship {
  name: string;
  model: string;
  manufacturer: string;
  max_atmosphering_speed: string;
  url: string;
}

export const useStarships = (page: number) => {
  const [starships, setStarships] = useState<Starship[]>([]);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);

  useEffect(() => {
    (async function () {
      try {
        setError(null);
        setLoading(true);
        const response = await axios.get(
          baseUrl + "starships/" + `?page=${page}`
        );
        if (response.data.Error) {
          setError(response.data.error);
          setStarships([]);
        } else {
          setCount(response.data.count);
          setStarships(response.data.results || []);
        }
      } catch (err) {
        if (axios.isAxiosError(err)) {
          setError(
            String(err.response?.status) + " " + err.response?.statusText
          );
          setStarships([]);
        }
      } finally {
        setLoading(false);
      }
    })();
  }, [page]);

  return { starships, error, loading, count };
};
