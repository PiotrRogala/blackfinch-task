import axios from "axios";

import { useEffect, useState } from "react";
import { baseUrl } from "../consts/api";
import { Starship } from "./useStarships";

export const useStarshipDetails = (id: string | undefined) => {
  const [starship, setStarship] = useState<Starship>();
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!id) return;

    (async function () {
      try {
        setError(null);
        setLoading(true);
        const response = await axios.get(baseUrl + `starships/${id}`);
        if (response.data.Error) {
          setError(response.data.error);
        } else {
          setStarship(response.data);
        }
      } catch (err) {
        if (axios.isAxiosError(err)) {
          setError(
            String(err.response?.status) + " " + err.response?.statusText
          );
        }
      } finally {
        setLoading(false);
      }
    })();
  }, [id]);

  return { starship, loading, error };
};
