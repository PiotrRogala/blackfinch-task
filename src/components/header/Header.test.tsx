import { render, screen } from "@testing-library/react";
import Header from "./Header";
import { BrowserRouter } from "react-router-dom";
import menuRoutes from "../../consts/menuRoutes";

test("Renders Header", () => {
  render(<Header />, { wrapper: BrowserRouter });

  const homeLink = screen.getByRole("link", { name: "Home" });
  expect(homeLink).toBeInTheDocument();
  expect(homeLink).toHaveAttribute("href", menuRoutes.home);

  const notExistingUrlLink = screen.getByRole("link", {
    name: "Not existing url",
  });
  expect(notExistingUrlLink).toBeInTheDocument();
  expect(notExistingUrlLink).toHaveAttribute(
    "href",
    "/random-not-existing-url"
  );

  const cartLink = screen.getByRole("link", { name: "(0)" });
  expect(cartLink).toBeInTheDocument();
  expect(cartLink).toHaveAttribute("href", menuRoutes.cart);

  const img = screen.getByRole("img");
  expect(img).toHaveAttribute(
    "src",
    "https://www.pngall.com/wp-content/uploads/14/Yoda-PNG-Image.png"
  );
  expect(img).toHaveAttribute("alt", "yoda");
});
