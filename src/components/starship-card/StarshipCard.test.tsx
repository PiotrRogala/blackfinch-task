import { render, screen } from "@testing-library/react";
import StarshipCard from "./StarshipCard";
import { Starship } from "../../hooks/useStarships";
import menuRoutes from "../../consts/menuRoutes";
import { BrowserRouter } from "react-router-dom";

const testStarship: Starship = {
  name: "Star Destroyer",
  model: "Imperial I-class Star Destroyer",
  manufacturer: "Kuat Drive Yards",
  max_atmosphering_speed: "975",
  url: "https://swapi.dev/api/starships/3/",
};

test("Renders StarshipCard", () => {
  render(<StarshipCard starship={testStarship} />, { wrapper: BrowserRouter });
  const link = screen.getByRole("link");
  expect(link).toBeInTheDocument();
  expect(link).toHaveAttribute("href", menuRoutes.details.replace(":id", "3"));

  const name = screen.getByText(testStarship.name);
  expect(name).toBeInTheDocument();

  const model = screen.getByText(testStarship.model);
  expect(model).toBeInTheDocument();

  const manufacturer = screen.getByText(testStarship.manufacturer);
  expect(manufacturer).toBeInTheDocument();

  const speed = screen.getByText(testStarship.max_atmosphering_speed + " km/h");
  expect(speed).toBeInTheDocument();

  const img = screen.getByRole("img");
  expect(img).toHaveAttribute("src", "/ship.png");
  expect(img).toHaveAttribute("alt", testStarship.name);
});
