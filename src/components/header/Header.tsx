import "./Header.css";

import { useState } from "react";

import { Link } from "react-router-dom";
import { Home, Cube, Menu as MenuIcon, CartAlt } from "iconoir-react";

import menuRoutes from "../../consts/menuRoutes";
import { slide as Menu } from "react-burger-menu";
import { useCart } from "../../hooks/useCart";

const Header = () => {
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const { itemsAmount } = useCart();

  return (
    <div className="header-container">
      <div className="desktop-menu">
        <Link to={menuRoutes.home} className="header-link">
          <Home />
          Home
        </Link>
        <Link to="random-not-existing-url" className="header-link">
          <Cube />
          Not existing url
        </Link>
      </div>
      <div className="mobile-menu">
        <Menu
          noOverlay
          disableOverlayClick
          customBurgerIcon={
            <MenuIcon onClick={() => setIsMobileMenuOpen((value) => !value)} />
          }
          isOpen={isMobileMenuOpen}
        >
          <Link to={menuRoutes.home} className="header-link">
            <Home />
            Home
          </Link>
          <Link to="random-not-existing-url" className="header-link">
            <Cube />
            Not existing url
          </Link>
        </Menu>
      </div>
      <img
        className="yoda-image"
        src="https://www.pngall.com/wp-content/uploads/14/Yoda-PNG-Image.png"
        alt="yoda"
      />
      <Link to={menuRoutes.cart} className="header-link">
        <CartAlt /> ({itemsAmount})
      </Link>
    </div>
  );
};

export default Header;
