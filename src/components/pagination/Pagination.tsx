import "./Pagination.css";

import { NavArrowLeft, NavArrowRight } from "iconoir-react";

interface PropTypes {
  currentPage: number;
  pages?: number;
  onPageChange: (value: number) => void;
}

const Pagination = ({ currentPage, pages, onPageChange }: PropTypes) => {
  return pages ? (
    <div className="pagination-container">
      {currentPage > 1 && (
        <NavArrowLeft onClick={() => onPageChange(currentPage - 1)} />
      )}
      <p>
        {currentPage} / {pages}
      </p>
      {currentPage < pages && (
        <NavArrowRight onClick={() => onPageChange(currentPage + 1)} />
      )}
    </div>
  ) : null;
};

export default Pagination;
