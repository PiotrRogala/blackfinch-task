import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import menuRoutes from "./consts/menuRoutes";
import Movies from "./pages/starships/Starships";
import MovieDetails from "./pages/starship-details/StarshipDetails";
import ErrorPage from "./pages/error/ErrorPage";
import Cart from "./pages/cart/Cart";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const router = createBrowserRouter([
  {
    path: menuRoutes.home,
    element: <Movies />,
    errorElement: <ErrorPage />,
  },
  {
    path: menuRoutes.details,
    element: <MovieDetails />,
  },
  {
    path: menuRoutes.cart,
    element: <Cart />,
  },
]);

function App() {
  return (
    <div className="App">
      <ToastContainer />
      <RouterProvider router={router}></RouterProvider>
    </div>
  );
}

export default App;
