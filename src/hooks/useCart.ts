import { useEffect, useState } from "react";
import { toast } from "react-toastify";

export type CartItem = {
  name: string;
  amount: number;
};

export type CartType = CartItem[];

export const useCart = () => {
  const [cart, setCart] = useState<CartType>([]);
  const [itemsAmount, setItemsAmount] = useState(0);

  const addToCart = (name: string, amount: number) => {
    const cart: CartType = JSON.parse(localStorage.getItem("cart") || "[]");
    const cartProduct = cart.find((p) => p.name === name);
    if (cartProduct) {
      cartProduct.amount = cartProduct.amount + amount;
    } else {
      cart.push({ name: name, amount: amount });
    }

    localStorage.setItem("cart", JSON.stringify(cart));
    const event = new Event("cartUpdated");
    document.dispatchEvent(event);
    toast(`${amount}x ${name} added to cart!`);
  };

  const clearCart = () => {
    localStorage.setItem("cart", JSON.stringify([]));
    const event = new Event("cartUpdated");
    document.dispatchEvent(event);
    toast("Cart cleared!");
  };

  useEffect(() => {
    const handleCartUpdatedEvent = () => {
      const cart: CartType = JSON.parse(localStorage.getItem("cart") || "[]");
      setCart(cart);
      setItemsAmount(
        cart.reduce((prev, current) => {
          return prev + current.amount;
        }, 0)
      );
    };

    handleCartUpdatedEvent();
    document.addEventListener("cartUpdated", handleCartUpdatedEvent);

    return () => {
      document.removeEventListener("cartUpdated", handleCartUpdatedEvent);
    };
  }, []);

  return { cart, itemsAmount, addToCart, clearCart };
};
