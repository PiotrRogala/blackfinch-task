import "./Cart.css";

import Header from "../../components/header/Header";
import { useCart } from "../../hooks/useCart";
import { BinMinusIn } from "iconoir-react";

const Cart = () => {
  const { cart, clearCart, itemsAmount } = useCart();
  return (
    <div>
      <Header />
      <>
        <h1 className="cart-header">Cart</h1>
        <div className="cart-details-container">
          <ul>
            {cart.map((item) => (
              <li key={item.name}>
                <h3>name: {item.name}</h3>
                <p>amount: {item.amount}</p>
              </li>
            ))}
          </ul>
          {itemsAmount > 0 ? (
            <button className="cart-clear-button" onClick={clearCart}>
              <BinMinusIn />
              Clear cart
            </button>
          ) : (
            <p>Cart is empty</p>
          )}
        </div>
      </>
    </div>
  );
};

export default Cart;
