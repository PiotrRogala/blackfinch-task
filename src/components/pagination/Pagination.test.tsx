import { render, screen } from "@testing-library/react";
import Pagination from "./Pagination";

test("Renders Pagination", () => {
  const change = jest.fn();
  render(<Pagination pages={4} currentPage={2} onPageChange={change} />);

  const currentPageText = screen.getByText("2 / 4");
  const icons = document.querySelectorAll("svg");

  expect(currentPageText).toBeInTheDocument();
  expect(icons).toHaveLength(2);
});
