import "./ErrorPage.css";

import { Home } from "iconoir-react";
import { Link } from "react-router-dom";

import menuRoutes from "../../consts/menuRoutes";

const ErrorPage = () => {
  return (
    <div className="error-page-container">
      <h1 className="error-header">Page not found</h1>
      <Link to={menuRoutes.home} className="error-page-link">
        <Home />
        Navigate to home
      </Link>
    </div>
  );
};

export default ErrorPage;
